const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const { user_game } = require('../models');

passport.use(new LocalStrategy(
  async (username, password, done) => {
    try {
      const user = await user_game.findOne({
        where: { username: username }
      });
      
      if (!user) {
        return done(null, false, { message: 'Invalid username or password' });
      }
      
      const isPasswordValid = await user.isValidPassword(password);
      
      if (!isPasswordValid) {
        return done(null, false, { message: 'Invalid username or password' });
      }
      
      return done(null, user);
    } catch (error) {
      return done(error);
    }
  }
));

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
  try {
    const user = await user_game.findByPk(id); // Retrieve user directly by primary key
    done(null, user);
  } catch (error) {
    done(error);
  }
});

module.exports = passport;
