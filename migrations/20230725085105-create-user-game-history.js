'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('user_game_history', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_key: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'user_game', // This references the 'user_game' table
          key: 'id', // This references the primary key field in the 'user_game' table
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      totalmatches: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
        allowNull: true
      },
      totalwins: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
        allowNull: true
      },
      totallosses: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
        allowNull: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('user_game_history');
  }
};
