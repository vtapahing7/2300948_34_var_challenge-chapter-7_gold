'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('rooms', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      player1_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: {
          model: 'user_game', // Name of the User_Game model
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      player2_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: {
          model: 'user_game', // Name of the User_Game model
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      player1Choice: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      player2Choice: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      room_key: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        unique: true,
        allowNull: false,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('rooms');
  },
};
