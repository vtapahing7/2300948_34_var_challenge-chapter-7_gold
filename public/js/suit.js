class Game {
  constructor() {
    this.choices = ["rock", "paper", "scissors"];
    this.resultImage = document.getElementById("result-image");
    this.playerButtonIds = ["rock", "paper", "scissors"];
    this.computerButtonIds = [
      "computer-rock",
      "computer-paper",
      "computer-scissors",
    ];

    this.resetPlayerButtonColors();
    this.addEventListeners();
  }

  generateRandomIndex() {
    return Math.floor(Math.random() * this.choices.length);
  }

  getComputerChoice() {
    const randomIndex = this.generateRandomIndex();
    return this.choices[randomIndex];
  }

  determineWinner(player1Choice, player2Choice) {
    if (player1Choice === player2Choice) {
      return "tie";
    } else if (
      (player1Choice === "rock" && player2Choice === "scissors") ||
      (player1Choice === "paper" && player2Choice === "rock") ||
      (player1Choice === "scissors" && player2Choice === "paper")
    ) {
      return "player1"; // Player 1 wins
    } else {
      return "player2"; // Player 2 wins
    }
  }

  handleClick(userChoice) {
    const computerChoice = this.getComputerChoice();
    const result = this.determineWinner(userChoice, computerChoice);

    console.log("You chose: " + userChoice);
    console.log("Computer chose: " + computerChoice);
    console.log("Result: " + result);

    this.resultImage.style.display = "block";

    if (result === "win") {
      this.resultImage.src = "assets/win.png";
    } else if (result === "lose") {
      this.resultImage.src = "assets/lose.png";
    } else if (result === "tie") {
      this.resultImage.src = "assets/tie.png";
    } else {
      this.resultImage.src = " ";
    }

    this.resetPlayerButtonColors();

    if (userChoice === "rock") {
      this.changeButtonColor("rock", "white");
    } else if (userChoice === "paper") {
      this.changeButtonColor("paper", "white");
    } else if (userChoice === "scissors") {
      this.changeButtonColor("scissors", "white");
    }

    this.changeButtonColor(userChoice, "white");

    if (computerChoice === "rock") {
      this.changeButtonColor("computer-rock", "white");
    } else if (computerChoice === "paper") {
      this.changeButtonColor("computer-paper", "white");
    } else if (computerChoice === "scissors") {
      this.changeButtonColor("computer-scissors", "white");
    }
  }

  resetPlayerButtonColors() {
    this.playerButtonIds.forEach((buttonId) => {
      this.changeButtonColor(buttonId, "");
    });

    this.computerButtonIds.forEach((buttonId) => {
      this.changeButtonColor(buttonId, "");
    });
  }

  changeButtonColor(id, color) {
    document.getElementById(id).style.backgroundColor = color;
  }

  restartGame() {
    this.resultImage.style.display = "none";
    this.resultImage.src = "";

    this.resetPlayerButtonColors();
    console.clear();
  }

  addEventListeners() {
    this.playerButtonIds.forEach((buttonId) => {
      document.getElementById(buttonId).addEventListener("click", () => {
        this.handleClick(buttonId);
      });
    });
    document.getElementById("restart").addEventListener("click", () => {
      this.restartGame();
    });
  }
  
}

const game = new Game();
