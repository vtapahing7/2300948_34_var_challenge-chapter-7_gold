class Game {
    constructor(player1Id, player2Id) {
      this.choices = ["rock", "paper", "scissors"];
      this.player1ButtonIds = [
        "player1-rock",
        "player1-paper",
        "player1-scissors",
      ];
      this.player2ButtonIds = [
        "player2-rock",
        "player2-paper",
        "player2-scissors",
      ];
      this.player1Id = player1Id;
      this.player2Id = player2Id;
  
      this.resetPlayerButtonColors();
      this.addEventListeners();
    }
    
  determineWinner(player1Choice, player2Choice) {
    if (player1Choice === player2Choice) {
      return "tie";
    } else if (
      (player1Choice === "rock" && player2Choice === "scissors") ||
      (player1Choice === "paper" && player2Choice === "rock") ||
      (player1Choice === "scissors" && player2Choice === "paper")
    ) {
      return "player1"; // Player 1 wins
    } else {
      return "player2"; // Player 2 wins
    }
  }
  handleClick(playerId, choice) {
    const opponentId =
      playerId === this.player1Id ? this.player2Id : this.player1Id;
    const opponentChoice = this.getOpponentChoice(opponentId); // You need to implement this function
    const result = this.determineWinner(choice, opponentChoice);

    console.log(`Player ${playerId} chose: ${choice}`);
    console.log(`Player ${opponentId} chose: ${opponentChoice}`);
    console.log(`Result: ${result}`);
  }
  addEventListeners() {
    // Add event listeners for player 1's buttons
    this.player1ButtonIds.forEach((buttonId) => {
      document.getElementById(buttonId).addEventListener("click", () => {
        this.handlePlayerClick(
          this.player1Id,
          buttonId.replace("player1-", "")
        );
      });
    });

    // Add event listeners for player 2's buttons
    this.player2ButtonIds.forEach((buttonId) => {
      document.getElementById(buttonId).addEventListener("click", () => {
        this.handlePlayerClick(
          this.player2Id,
          buttonId.replace("player2-", "")
        );
      });
    });
  }
  async  handlePlayerClick(playerId, choice) {
    // Implement logic to handle player's button click
    // This is where you would call the handleClick method
    // and potentially update the UI or communicate with the server
    document.getElementById(`player${playerId}-${choice}`).classList.add("selected");
    this.handleClick(playerId, choice);
    if (result === 'player1') {
        await this.updateUserGameStats(this.player1Id, 'totalwins');
        await this.updateUserGameStats(this.player2Id, 'totallosses');
      } else if (result === 'player2') {
        await this.updateUserGameStats(this.player2Id, 'totalwins');
        await this.updateUserGameStats(this.player1Id, 'totallosses');
      }
  
      await this.updateUserGameStats(this.player1Id, 'totalmatches');
      await this.updateUserGameStats(this.player2Id, 'totalmatches');
    }

    async updateUserGameStats(userId, statField) {
        try {
          // Find the user's record in the database
          const user = await UserGame.findByPk(userId);
    
          if (!user) {
            console.error(`User with ID ${userId} not found.`);
            return;
          }
    
          // Increment the specified statistic field
          await user.increment(statField);
    
          console.log(`Updated ${statField} for User ID ${userId}.`);
        } catch (error) {
          console.error(`Error updating user stats: ${error}`);
        }
      }
}
