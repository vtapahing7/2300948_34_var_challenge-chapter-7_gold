"use strict";
const { Model } = require('sequelize');
const user_game = require("./user_game"); // Update the path to the correct location of the user_game model

module.exports = (sequelize, DataTypes) => {
  class user_game_history extends Model {
    static associate(models) {
      // define association here
      user_game_history.belongsTo(models["user_game"], {
        foreignKey: "user_key", // This references the uuid field in the 'user-game' model
        as: "user_game_history", // Alias to access the associated 'user-game' model
      });
    }
  }
  user_game_history.init(
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      user_key: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      totalmatches: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
        allowNull: true
      },
      totalwins: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
        allowNull: true
      },
      totallosses: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
        allowNull: true
      },
    },
    {
      sequelize,
      tableName: "user_game_history",
      modelName: "user_game_history",
    }
  );
  return user_game_history;
};
