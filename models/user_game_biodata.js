'use strict';
const { Model } = require('sequelize');
const user_game = require("./user_game"); // Update the path to the correct location of the user_game model

module.exports = (sequelize, DataTypes) => {
  class user_game_biodata extends Model {
    static associate(models) {
      // define association here
      user_game_biodata.belongsTo(models["user_game"], {
        foreignKey: "user_key", // This references the uuid field in the 'user-game' model
        as: "user_game_biodata", // Alias to access the associated 'user-game' model
      });
    }
  }

  user_game_biodata.init({
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    user_key: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    fullname: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    religion: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    address: {
      type: DataTypes.STRING,
      allowNull: true,
    },
  }, {
    sequelize,
    tableName: "user_game_biodata",
    modelName: 'user_game_biodata',
  });

  return user_game_biodata;
};
