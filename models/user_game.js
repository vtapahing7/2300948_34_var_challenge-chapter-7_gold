"use strict";
const { Model } = require("sequelize");
const bcrypt = require("bcrypt");
module.exports = (sequelize, DataTypes) => {
  class user_game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    async isValidPassword(password) {
      return bcrypt.compare(password, this.password);
    }

    static associate(models) {
      // define association here
      user_game.hasOne(models.user_game_biodata, {
        foreignKey: "user_key",
        as: "user_game_biodata",
      });
      user_game.hasOne(models.user_game_history, {
        foreignKey: "user_key",
        as: "user_game_history",
      });
    }
  }
  user_game.init(
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      username: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      role: {
        type: DataTypes.STRING, // Use the appropriate data type for roles
        allowNull: false,
        defaultValue: "guest", // Set the default role value
      },
    },
    {
      sequelize,
      tableName: "user_game",
      modelName: "user_game",
    }
  );
  return user_game;
};
