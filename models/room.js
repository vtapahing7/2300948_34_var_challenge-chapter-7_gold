const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class Room extends Model {
    static associate(models) {
      Room.belongsTo(models.user_game, {
        foreignKey: "player1_id",
        as: "player1",
      });

      Room.belongsTo(models.user_game, {
        foreignKey: "player2_id",
        as: "player2",
      });
    }
    static async createRoom(player1_id, player2_id, room_key) {
      try {
        if (player1_id === player2_id) {
          throw new Error("Player 1 and Player 2 cannot be the same");
        }

        const room = await this.create({
          player1_id,
          player2_id,
          room_key,
        });

        return room;
      } catch (error) {
        throw error;
      }
    }
  }

  Room.init(
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      player1_id: {
        type: DataTypes.INTEGER,
        allowNull: true,
        validate: {
          notEqual: function (value) {
            if (value === this.getDataValue("player2_id")) {
              throw new Error("Player 1 and Player 2 cannot be the same");
            }
          },
        },
      },
      player2_id: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      player1Choice: {
        type: DataTypes.STRING, // or however you want to store the choice
        allowNull: true,
      },
      player2Choice: {
        type: DataTypes.STRING, // or however you want to store the choice
        allowNull: true,
      },
      room_key: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        unique: true,
        allowNull: false,
      },
    },
    {
      sequelize,
      tableName: "rooms",
      modelName: "Room",
      timestamp: false,
    }
  );

  return Room;
};