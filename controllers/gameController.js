// gameController.js

const { user_game, user_game_history } = require("../models"); // Update the path to your user_game model
const { Room } = require("../models");

class GameController {
  static async updateUserGameStats(userId, statField) {
    try {
      const user = await user_game.findByPk(userId);

      if (!user) {
        console.error(`User with ID ${userId} not found.`);
        return;
      }

      await user.increment(statField);

      console.log(`Updated ${statField} for User ID ${userId}.`);
    } catch (error) {
      console.error(`Error updating user stats: ${error}`);
    }
  }

  static async handleGameResult(player1Id, player2Id, result) {
    if (result === "player1") {
      await this.updateUserGameStats(player1Id, "totalwins");
      await this.updateUserGameStats(player2Id, "totallosses");
    } else if (result === "player2") {
      await this.updateUserGameStats(player2Id, "totalwins");
      await this.updateUserGameStats(player1Id, "totallosses");
    }

    await this.updateUserGameStats(player1Id, "totalmatches");
    await this.updateUserGameStats(player2Id, "totalmatches");
  }

  static async startGame(req, res) {
    // Add models as an argument
    try {
      const roomId = req.params.id;
      const room = await Room.findByPk(roomId);
      //   room.player1Choice = "rock"; // Replace with actual player 1 choice
      // room.player2Choice = "scissor"; // Replace with actual player 2 choice
      res.render("pages/game", { room });
    } catch (error) {
      console.error("Error starting game:", error);
      res.status(400).redirect("/rooms");
    }
  }
  static async playGame(req, res) {
    try {
      const userChoice = req.body.choice;
      const roomId = req.params.id;

      const room = await Room.findByPk(roomId);

      if (!room.player1Choice) {
        // Set the user's choice as player1's choice
        room.player1Choice = userChoice;
        await room.save();
        res.redirect("/rooms");
      } else if (!room.player2Choice) {
        // Set the user's choice as player2's choice
        room.player2Choice = userChoice;
        await room.save();
        res.redirect("/rooms");
      } else {
        {
          return res.redirect("/rooms");
        }
      }
      

      if (room.player1Choice && room.player2Choice) {
        // Determine the winner and perform necessary actions
        let winner = "draw";
        if (
          (room.player1Choice === "rock" &&
            room.player2Choice === "scissors") ||
          (room.player1Choice === "paper" && room.player2Choice === "rock") ||
          (room.player1Choice === "scissors" && room.player2Choice === "paper")
        ) {
          winner = "player1";
        } else if (
          (room.player2Choice === "rock" &&
            room.player1Choice === "scissors") ||
          (room.player2Choice === "paper" && room.player1Choice === "rock") ||
          (room.player2Choice === "scissors" && room.player1Choice === "paper")
        ) {
          winner = "player2";
        }

        // Update room and player history
        if (winner !== "draw") {
          const player1History = await user_game_history.findOne({
            where: { user_key: room.player1_id },
          });
          const player2History = await user_game_history.findOne({
            where: { user_key: room.player2_id },
          });

          if (winner === "player1") {
            player1History.increment("totalwins");
            player2History.increment("totallosses");
          } else if (winner === "player2") {
            player2History.increment("totalwins");
            player1History.increment("totallosses");
          }

          player1History.increment("totalmatches");
          player2History.increment("totalmatches");

          await player1History.save();
          await player2History.save();
        }

        // Reset player choices for the next game round
        room.player1Choice = null;
        room.player2Choice = null;
        await room.save();
      }
      // res.redirect("/rooms");
    } catch (error) {
      console.error("Error playing game:", error);
      // res.status(400).redirect("/rooms");
    }
  }

  static async showGameResults(req, res) {
    try {
      const roomId = req.params.id;
      const room = await Room.findByPk(roomId);

      if (!room) {
        return res.redirect("/rooms", { errorMessage: "Room not found" });
      }

      const { player1Choice, player2Choice, winner } = room;

      res.render("pages/game-results", {
        player1Choice,
        player2Choice,
        winner,
      });
    } catch (error) {
      console.error("Error showing game results:", error);
      res.status(400).redirect("/rooms");
    }
  }
}

module.exports = GameController;
