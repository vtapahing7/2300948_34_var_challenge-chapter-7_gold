const {
  user_game,
  user_game_biodata,
  user_game_history,
} = require("../models");
const passport = require("../lib/passport");
const bcrypt = require("bcrypt");

const UserController = {
  async getUsers(req, res) {
    try {
      const users = await user_game.findAll({
        include: [
          { model: user_game_biodata, as: "user_game_biodata" },
          { model: user_game_history, as: "user_game_history" },
        ],
      });

      res.render("pages/dashboard", { users });
    } catch (error) {
      console.error("Error fetching users:", error);
      res.status(500).send("Internal Server Error: " + error.message);
    }
  },

  index(req, res) {
    res.render("pages/index");
  },

  renderLogin(req, res) {
    res.render("pages/login");
  },

  renderWelcome(req, res) {
    res.render("pages/welcome");
  },

  renderRegister(req, res) {
    res.render("pages/register");
  },

  async registerUser(req, res) {
    try {
      const {
         username,
         email,
        password,
         isAdmin,
         fullname,
        religion,
        address,
        totalwins,
        totallosses,
        totalmatches
          } = req.body;
  
      // Check if the username is already taken
      const existingUser = await user_game.findOne({
        where: { username },
      });
  
      if (existingUser) {
        return res.render("pages/register", {
          errorMessage: "Username already taken",
        });
      }
  
      // Check if the email is already taken
      const existingEmail = await user_game.findOne({
        where: { email },
      });
  
      if (existingEmail) {
        return res.render("pages/register", {
          errorMessage: "Email already registered",
        });
      }
  
      // Hash the password
      const hashedPassword = await bcrypt.hash(password, 10);
  
      // Determine the role based on isAdmin checkbox
      const role = isAdmin ? "admin" : "guest";
  
      // Create new user
      const newUser = await user_game.create({
        username,
        email,
        password: hashedPassword,
        role,
        user_game_biodata: {
          fullname,
          religion,
          address,
        },
        user_game_history: {
          totalmatches,
          totalwins,
          totallosses,
        }
      },
        {
          include: [
            { model: user_game_biodata, as: "user_game_biodata" },
            { model: user_game_history, as: "user_game_history" },
          ],
      });
  
      // Redirect to a success page or display a success message
      return res.redirect("/login");
    } catch (error) {
      console.error("Error registering user:", error);
      return res.redirect("/register", {
        errorMessage: "An error occurred during registration",
      });
    }
  },
  
  // Handle user login
login(req, res, next) {
  passport.authenticate("local", (err, user, info) => {
    if (err) {
      return next(err);
    }
    if (!user) {
      return res.redirect("/login");
    }
    
    // Continue with the authentication and store the user in the session
    req.logIn(user, (err) => {
      if (err) {
        return next(err);
      }

      // Redirect based on user's role
      if (user.role === "admin") {
        return res.redirect("/users");
      } else if (user.role === "guest") {
        return res.redirect("/rooms");
      }
    });
  })(req, res, next);
},
  
  async getUserDetails(req, res) {
    try {
      const user = await user_game.findOne({
        where: { id: req.params.id },
        include: [
          { model: user_game_biodata, as: "user_game_biodata" },
          { model: user_game_history, as: "user_game_history" },
        ],
      });

      if (!user) {
        return res.status(404).send("user_game not found");
      }

      res.render("pages/details", { user });
    } catch (error) {
      console.error("Error fetching user details:", error);
      res.status(500).send("Internal Server Error: " + error.message);
    }
  },

  async renderCreateUser (req, res)  {
    try {
      res.render("pages/create"); // Replace "pages/create-user" with the actual path to your create user form view
    } catch (error) {
      console.error("Error rendering create user page:", error);
      res.status(500).send("Internal Server Error: " + error.message);
    }
  },

  async createUser(req, res) {
    try {
      const {
        username,
        email,
        password,
        role,
        fullname,
        religion,
        address,
        totalwins,
        totallosses,
        totalmatches
      } = req.body;

      const newUser = await user_game.create(
        {
          username,
          email,
          password,
          role,
          user_game_biodata: {
            fullname,
            religion,
            address,
          },
          user_game_history: {
            totalmatches,
            totalwins,
            totallosses,
          },
        },
        {
          include: [
            { model: user_game_biodata, as: "user_game_biodata" },
            { model: user_game_history, as: "user_game_history" },
          ],
        }
      );

      res.redirect("/users");
    } catch (error) {
      console.error("Error creating user:", error);
      res.status(500).send("Internal Server Error: " + error.message);
    }
  },

  editUserForm: async (req, res) => {
    try {
      const user = await user_game.findOne({
        where: { id: req.params.id },
        include: [
          { model: user_game_biodata, as: "user_game_biodata" },
          { model: user_game_history, as: "user_game_history" },
        ],
      });

      if (!user) {
        return res.status(404).send("User not found");
      }

      res.render("pages/update", { user });
    } catch (error) {
      console.error("Error fetching user details:", error);
      res.status(500).send("Internal Server Error: " + error.message);
    }
  },

  updateUser: async (req, res) => {
    try {
      const {
        username,
        email,
        password,
        role,
        fullname,
        religion,
        address,
        totalmatches,
        totalwins,
        totallosses,
      } = req.body;
      const userId = req.params.id;

      await user_game.update(
        { username, email, password, role },
        { where: { id: userId } }
      );

      let biodata = await user_game_biodata.findOne({
        where: { user_key: userId },
      });
      if (biodata) {
        await biodata.update({
          fullname,
          religion,
          address,
        });
      } else {
        biodata = await user_game_biodata.create({
          user_key: userId,
          fullname,
          religion,
          address,
        });
      }

      let userHistory = await user_game_history.findOne({
        where: { user_key: userId },
      });
      if (userHistory) {
        await userHistory.update({
          totalmatches,
          totalwins,
          totallosses,
        });
      } else {
        userHistory = await user_game_history.create({
          user_key: userId,
          totalmatches,
          totalwins,
          totallosses,
        });
      }

      res.redirect(`/users/${userId}/edit`);
    } catch (error) {
      console.error("Error updating user:", error);
      res.status(500).send("Internal Server Error: " + error.message);
    }
  },

  deleteUserForm: async (req, res) => {
    try {
      const user = await user_game.findOne({ where: { id: req.params.id } });
      if (!user) {
        return res.status(404).send("User not found");
      }

      res.render("pages/delete", { user });
    } catch (error) {
      console.error("Error fetching user details:", error);
      res.status(500).send("Internal Server Error: " + error.message);
    }
  },

  deleteUser: async (req, res) => {
    try {
      const user = await user_game.findOne({
        where: { id: req.params.id },
        include: [
          { model: user_game_biodata, as: "user_game_biodata" },
          { model: user_game_history, as: "user_game_history" },
        ],
      });

      if (!user) {
        return res.status(404).send("User not found");
      }

      if (user.user_game_biodata) {
        await user.user_game_biodata.destroy();
      }

      if (user.user_game_history) {
        await user.user_game_history.destroy();
      }

      await user.destroy();

      res.redirect("/users");
    } catch (error) {
      console.error("Error deleting user:", error);
      res.status(500).send("Internal Server Error: " + error.message);
      console.error(error);
    }
  },
};

module.exports = UserController;
