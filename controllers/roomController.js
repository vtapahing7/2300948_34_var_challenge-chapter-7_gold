const { Room } = require("../models");
const { v4: uuidv4 } = require("uuid");

module.exports = {
  roomDashboard: async (req, res) => {
    try {
      // Retrieve all rooms from the database
      const rooms = await Room.findAll();

      // Render the room dashboard EJS template
      res.render("pages/roomdashboard", { rooms });
    } catch (error) {
      console.error("Error retrieving rooms:", error);
      res.render("pages/roomDashboard", {
        errorMessage: "An error occurred while retrieving rooms",
      });
    }
  },
  createGameRoom: async (req, res) => {
    try {
      const { userId } = req.session; // Get the logged-in user's ID
      const room = await Room.createRoom(userId, null, uuidv4()); // Create a new room
      res.redirect(`/rooms`);
    } catch (error) {
      console.error("Error creating game room:", error);
      res.status(500).send("Internal Server Error");
    }
  },

  deleteRoom: async (req, res) => {
    try {
      const roomId = req.params.id;
      await Room.destroy({
        where: { id: roomId },
      });
      res.redirect(`/rooms`);
    } catch (error) {
      console.error("Error deleting room:", error);
      res.status(500).send("Internal Server Error");
    }
  },

  joinRoom: async (req, res) => {
    const roomId = req.params.id;
  
    try {
      const room = await Room.findByPk(roomId);
      if (!room) {
        return res.redirect("/rooms");
      }
  
      const playerId = room.player1_id ? "player2_id" : "player1_id";
  
      if (room[playerId] === req.user.id) {
        return res.redirect("/rooms");
      }
  
      if (room[playerId] !== null) {
        return res.redirect("/rooms");
      }
  
      await room.update({ [playerId]: req.user.id });
  
      // Redirect to the game page with the correct roomId
      res.redirect(`/games/${roomId}`);
    } catch (error) {
      console.error("Error joining room:", error);
      res.status(400).redirect("/rooms");
    }
  },
  


  // ... other controller methods
};
