const express = require('express');
const ejs = require('ejs');
const app = express();
const path = require('path');
const { sequelize } = require('./models');
const methodOverride = require('method-override');
const session = require('express-session');
const flash = require('connect-flash');


const passport = require('./lib/passport'); // Import passport configuration
const UserController = require('./controllers/userController'); // Import UserController


app.use(express.static('public'));
app.use(express.urlencoded({ extended: false }));
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.use(express.json());
app.use(methodOverride('_method'));

app.use(session({
  secret: 'your-secret-key',
  resave: false,
  saveUninitialized: false,
}));

app.use(passport.initialize());
app.use(passport.session());


// Use the userRoutes for all user-related routes
const roomRoutes = require('./routes/roomRoutes');
const userRoutes = require('./routes/userRoutes');
app.use(userRoutes);
app.use(roomRoutes);

app.use(flash());

sequelize.sync({ force: false })
  .then(() => {
    console.log('Database synced successfully.');
  })
  .catch((error) => {
    console.error('Unable to sync database:', error);
  });

app.listen(3000, () => {
  console.log('Server started on port 3000');
});
