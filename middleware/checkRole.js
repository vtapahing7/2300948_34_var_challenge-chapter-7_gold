const checkRole = (requiredRole) => {
    return (req, res, next) => {
      if (req.user && req.user.role === requiredRole) {
        next(); // User has the required role, allow access
      } else {
        res.status(403).json({ error: 'Access forbidden' }); // User doesn't have required role
      }
    };
  };
  
  module.exports = checkRole;
  