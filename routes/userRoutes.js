const express = require('express');
const router = express.Router();
const UserController = require('../controllers/userController');
const { ensureAuthenticated } = require('../middleware/auth');
const checkRole = require('../middleware/checkRole')


router.get('/', UserController.index);
router.get('/welcome', ensureAuthenticated, UserController.renderWelcome);
router.get('/users', ensureAuthenticated, checkRole('admin'), UserController.getUsers);
router.get('/users/:id', ensureAuthenticated, checkRole('admin'), UserController.getUserDetails);
router.get('/users/:id/edit', ensureAuthenticated, checkRole('admin'), UserController.editUserForm);
router.get('/users/:id/delete', ensureAuthenticated, checkRole('admin'), UserController.deleteUserForm);
router.get('/users/new', ensureAuthenticated, checkRole('admin'), UserController.renderCreateUser);

router.post('/users/new', ensureAuthenticated, checkRole('admin'), UserController.createUser);
router.post('/users/:id', ensureAuthenticated, checkRole('admin'), UserController.updateUser);
router.post('/users/:id/edit', ensureAuthenticated, checkRole('admin'), UserController.updateUser);
router.post('/users/:id/deletedata', ensureAuthenticated, checkRole('admin'), UserController.deleteUser);

router.get('/register', UserController.renderRegister);
router.get('/login', UserController.renderLogin);
router.post('/login', UserController.login);
router.post('/register', UserController.registerUser);


module.exports = router;
