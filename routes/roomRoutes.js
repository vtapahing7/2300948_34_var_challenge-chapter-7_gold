const express = require('express');
const router = express.Router();
const roomController = require('../controllers/roomController');
const gameController = require('../controllers/gameController');

// Middleware to ensure authentication (similar to what you've used before)
const { ensureAuthenticated } = require('../middleware/auth');

router.get('/rooms', ensureAuthenticated, roomController.roomDashboard);
router.post('/rooms/create', ensureAuthenticated, roomController.createGameRoom);
router.post('/rooms/delete/:id', ensureAuthenticated, roomController.deleteRoom);
router.post('/rooms/join/:id',ensureAuthenticated, roomController.joinRoom)

router.get('/games/:id', ensureAuthenticated, gameController.startGame) // Pass models as an argumen
router.post('/games/:id/choice', ensureAuthenticated, gameController.playGame);
router.get('/games/:id/results', ensureAuthenticated, gameController.showGameResults) // Pass models as an argumen


      
// Add other routes using roomController methods

module.exports = router;
