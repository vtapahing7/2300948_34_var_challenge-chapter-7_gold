# Step by Step Instructions 

## 1 Create Database and Install necessary node package

Use this command on the terminal to install the necessary Node Package, create database and migrate all the models to the database

npm install
sequelize db:create
sequelize db:migrate


## 2 Open the port http://localhost:3000/ on the browser 

navigate to the Sign Up button on the top right side of the landing page. it will lead you to the register form

## 3 Register your data

Fill the Username, Email and password form. you can choose to either fill the admin checkbox. after you are done filling the form your data
would be registered on the user_game, user_game_biodata and user_game_history

## 4 Login

after you fill the register form and click the register button, you will be redirected to the /login endpoint where you can login
using the previous username and password that you use to register. previously if you choose to register as an "admin" you would be redirected to the /users endpoint
after you logged in where you can view, add, edit and delete users from the user_game database. if you did not register as an admin your role would automatically be "guest"
and you would be redirected to the /rooms endpoint instead. if you are logged in as a user that have the role "guest" and open the /users endpoint there would be an error message
page with the message "Access Forbidden".

## 5 Managing Rooms

after you login proceed to the /rooms endpoint. at the /rooms endpoint there would be the roomdashboard page where you can create, delete and join a room as a user.
if you press the "Create Room" button, a new room would be created right under the button. you can choose to join or delete the room.
if you check the room database you would see there would be a new data created everytime you created a room.

## 6 Joining a room and Playing RPS

after you choose to join a room you would be redirected to the /games/:id endpoint where you can choose between three choices that is rock, paper or scissors. after you choose
your choice would be sent to the rooms database on the player1Choice column. after that register again and login as another user (or login on another browser), navigate to the room
that you have made before on the /rooms endpoint, choose between the three choices. before you choose if you check on the rooms database you would see on the "player1_id" & "player2_id"
columns filled with "id" according to each respective user_game "id". 

## 7 Game results

after both players make a choice, both player1Choice & player2Choice would be back to null again, this makes it so that both players can choose to play the game again on the same room.
if you check on the user_game_history database you would also see that each players totalwins, totallosses & totalmatch is recorded on each respective columns. you can also check this data on /users/:id endpoint.

## I hope this instructions is helpful

If there is anything wrong you can contact me. Thank you


